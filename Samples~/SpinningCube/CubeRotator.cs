﻿using rayd.oskar.content;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeController: OSKARObjectController
{
    [Space(10)]
    [SerializeField] private Transform rotateTrans;
    [SerializeField] private float speed = 1;
    [SerializeField] private Vector3 rotateAxis = Vector3.up;

    private bool shouldRotate;

    public override void DataReceived_Bool(string fieldName, bool data)
    {
        base.DataReceived_Bool(fieldName, data);
        if(fieldName == "rotate")
        {
            shouldRotate = data;
        }
    }

    void Update()
    {
        if (shouldRotate)
        {
            rotateTrans.Rotate(rotateAxis, Time.deltaTime * speed);
        }
    }
}
