using UnityEngine;
using rayd.oskar.content;
using UnityEngine.UI;

public class SampleLowerThirdController : OSKARObjectController
{
    [SerializeField] private TMPro.TextMeshProUGUI nameLabel;
    [SerializeField] private TMPro.TextMeshProUGUI ageLabel;
    [SerializeField] private RawImage nationImg;
    [SerializeField] private RawImage headshotImg;

    public override void DataReceived_String(string fieldName, string data)
    {
        base.DataReceived_String(fieldName, data);
        if(fieldName == "name")
        {
            nameLabel.text = data;
        }
        if(fieldName== "nation")
        {
            nationImg.texture=OSKAR_Statics.GetFlag(data);
        }
        RefreshVisibility();
    }
    public override void DataReceived_Int(string fieldName, int data)
    {
        base.DataReceived_Int(fieldName, data);
        if(fieldName == "age")
        {
            ageLabel.text = data.ToString();
        }
        RefreshVisibility();
    }
    public override void DataReceived_Texture(string fieldName, Texture2D texture)
    {
        base.DataReceived_Texture(fieldName, texture);
        if( fieldName == "headshot")
        {
            headshotImg.texture = texture;
        }
        RefreshVisibility();
    }

    private void RefreshVisibility()
    {
        nameLabel.gameObject.SetActive(!string.IsNullOrEmpty(nameLabel.text));
        ageLabel.gameObject.SetActive(!string.IsNullOrEmpty(ageLabel.text));
        headshotImg.gameObject.SetActive(headshotImg.texture != null);
        nationImg.gameObject.SetActive (nationImg.texture != null);
    }
}
