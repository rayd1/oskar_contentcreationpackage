using UnityEngine;
using rayd.oskar.content;
public class OSKAR_Broadcastsample_ListLine : OSKARObject_GenericUIListLine
{
    [SerializeField] private TMPro.TextMeshProUGUI nameLabel_dynamic;
    [SerializeField] private TMPro.TextMeshProUGUI ageLabel_dynamic;
    [SerializeField] private TMPro.TextMeshProUGUI pointLabel_dynamic;

    public override void SetText(string lineText)
    {
        base.SetText(lineText);
        //split the text and apply
        string[] parts = lineText.Split(";");
        if(parts.Length > 0)
        {
            nameLabel_dynamic.text = parts[0];
        }
        if(parts.Length > 1)
        {
            ageLabel_dynamic.text = parts[1];
        }
        if( parts.Length > 2)
        {
            pointLabel_dynamic.text = parts[2];
        }
    }

}
