using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace rayd.oskar
{
    [System.Serializable]
    public class Vector3_Serializable
    {
        public float x;
        public float y;
        public float z;

        public Vector3_Serializable(Vector3 v)
        {
            x = v.x;
            y = v.y;
            z = v.z;
        }

        public Vector3 GetVector3()
        {
            return new Vector3(x, y, z);
        }
    }
}