using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace rayd.oskar.content
{
    /// <summary>
    /// This is the scriptable Object to setup the data objects in Unity
    /// </summary>
    [CreateAssetMenu(fileName = "OSKARObjectData_ScriptableObject", menuName = "OSKAR/OSKARObject_Setup", order = 1)]
    public class OSKARObjectData_ScriptableObject : ScriptableObject
    {
        [SerializeField]
        public GameObject renderPrefab;
        public float defaultScale = 1f;
        public DataFieldBlueprint[] controlOptions;
        //public Texture2D tumbnailImage;

        
        public void Reset()
        {
            //always create show
            controlOptions = new DataFieldBlueprint[1];
            var showData = new DataFieldBlueprint();
            showData.controlName = "show";
            showData.defaultValue = "1";
            showData.additionalData = "IN\r\nOUT\r\nPREV";
            showData.controlType = DataFieldBlueprint.ControlTypes.RADIO;
            showData.visibleOnBaseElement  = true;
            controlOptions[0] = showData;

        }
        
    }
}