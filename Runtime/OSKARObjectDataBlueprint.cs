using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace rayd.oskar.content
{
    /// <summary>
    /// This is a blueprint class which represents an OSKARObject (Scriptable Object) but can be sent via Networking. Its a collection of DataieldBlueprints. This is basically a data prefab for the controls, the data is handled in OSKARObject_Setupdata
    /// </summary>
    [System.Serializable]
    public class OSKARObjectDataBlueprint
    {
        public string blueprintID;
        public DataFieldBlueprint[] controlOptions;
        public string tumbnailImageID;
        public string objectPrefabName;
        public float defaultScale;

        public static OSKARObjectDataBlueprint GetData(OSKARObjectData_ScriptableObject o)
        {
            OSKARObjectDataBlueprint data = new OSKARObjectDataBlueprint()
            {
                controlOptions = o.controlOptions,
                blueprintID = o.name,
                objectPrefabName = o.renderPrefab.name,
                defaultScale = o.defaultScale
            };
            /*
            if (o.tumbnailImage != null)
            {
                //get tumbnail id for ImageDB
                data.tumbnailImageID = o.tumbnailImage.name;
            }
            */
            return data;
        }
    }

    /// <summary>
    /// This is a blueprint class for one control option
    /// </summary>
    [System.Serializable]
    public class DataFieldBlueprint
    {
        public string controlName;
        public ControlTypes controlType;
        [Multiline(2)]
        public string defaultValue;
        [Multiline(2)]
        public string additionalData;
        public bool visibleOnBaseElement;
        public Vector2Int overrideControlSize;
        public enum ControlTypes
        {
            STRING,
            BOOL,
            INT,
            FLOAT,
            ENUM,
            INTRANGE,
            FLOATRANGE,
            TRIGGER,
            DBIMAGE,
            RADIO,
            VIDEO
        }

        public string GetDefaultValue()
        {
            switch (controlType)
            {
                case ControlTypes.STRING: return "empty";
                case ControlTypes.BOOL: return "false";
                case ControlTypes.INT: return "0";
                case ControlTypes.FLOAT: return "0";
                case ControlTypes.ENUM: return "0";
                case ControlTypes.INTRANGE: return "0";
                case ControlTypes.FLOATRANGE: return "0";
                case ControlTypes.TRIGGER: return "false";
                case ControlTypes.RADIO: return "0";
            }
            return null;
        }
    }

}