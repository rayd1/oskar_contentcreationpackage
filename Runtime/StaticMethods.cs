using UnityEngine;

namespace rayd.oskar.content
{
    public static class OSKAR_Statics  
    {        
        public static Texture2D GetFlag(string name)
        {
            Texture2D tex = Resources.Load<Texture2D>("FLAGS_BIG_IOC/" + name);
            if (tex != null)
            {
                tex.wrapMode = TextureWrapMode.Clamp;
            }
            return tex;
        }
    }
}
