using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace rayd.oskar.content
{
    /// <summary>
    /// This is the data for one control value of an object
    /// </summary>
    [System.Serializable]
    public class OSKARObject_FieldData
    {
        public string controlID;
        public string value;
        public DataFieldBlueprint.ControlTypes controlType;
    }

    /// <summary>
    /// This data stores which object is placed where - name/ID of the object, name/ID of the prefab/blueprint, name/ID of the anchor and all the Data Fields
    /// </summary>
    [System.Serializable]
    public class OSKARObject_SetupData
    {
        public string objectID;
        public string objectTypeID;
        public string anchorID;

        public List<OSKARObject_FieldData> values;

        public Vector3_Serializable positionOffset;
        public float scaleOffset = 1;
        public Vector3_Serializable rotationOffset;

        public OSKARObject_FieldData GetFieldData(string fieldName)
        {
            foreach (var field in values)
            {
                if (field.controlID == fieldName)
                {
                    return field;
                }
            }
            return null;
        }

        public void InitFieldData(string id, string value, DataFieldBlueprint.ControlTypes type)
        {
            var data = GetFieldData(id);
            if (data == null)
            {
                data = new OSKARObject_FieldData();
                data.controlID = id;
                data.controlType = type;
                values.Add(data);
            }
            data.value = value;
        }
        public void UpdateFieldData(string id, string value)
        {
            var data = GetFieldData(id);
            if (data != null)
            {
                if (data.controlType == DataFieldBlueprint.ControlTypes.DBIMAGE)
                {
                    value = value.ToLower();
                }
                data.value = value;
            }
        }

        public OSKARObject_SetupData()
        {
            values = new List<OSKARObject_FieldData>();

            positionOffset = new Vector3_Serializable(Vector3.zero);
            rotationOffset = new Vector3_Serializable(Vector3.zero);
        }

        public void ResetTriggers()
        {
            foreach (var field in values)
            {
                if (field.controlType == DataFieldBlueprint.ControlTypes.TRIGGER)
                {
                    field.value = "false";
                }
            }
        }

    }

    /// <summary>
    /// This represents a collection of setup Datas - this is sent via network to update all controls
    /// </summary>
    [System.Serializable]
    public class OSKARObject_SetupdataCollection
    {
        public List<OSKARObject_SetupData> datas;

        public OSKARObject_SetupdataCollection()
        {
            datas = new List<OSKARObject_SetupData>();
        }

        public OSKARObject_SetupData GetDataByID(string id)
        {
            foreach (var d in datas)
            {
                if (d.objectID == id)
                {
                    return d;
                }
            }
            return null;
        }
    }
}