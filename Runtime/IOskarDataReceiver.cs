using UnityEngine;

namespace rayd.oskar.content
{
    public interface IOskarDataReceiver
    {
        public virtual void DataReceived_String(string fieldName, string data)
        {

        }
        public virtual void DataReceived_Bool(string fieldName, bool data)
        {

        }
        public virtual void DataReceived_Int(string fieldName, int data)
        {

        }
        public virtual void DataReceived_Enum_Radio(string fieldName, int data)
        {

        }
        public virtual void DataReceived_Float(string fieldName, float data)
        {

        }
        public virtual void DataReceived_Trigger(string fieldName)
        {

        }
        public virtual void DataReceived_Texture(string fieldName, Texture2D texture)
        {

        }

        public virtual void DataReceived_Video(string fieldName, string videoName)
        {

        }
    }
}
