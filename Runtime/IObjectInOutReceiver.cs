using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace rayd.oskar.content
{
    public interface IObjectInOutReceiver
    {

        void ChangeInOut(OSKARObject_InOutController.INOUTState state);

    }
}