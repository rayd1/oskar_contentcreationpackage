using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace rayd.oskar.content
{
    public class OSKARObject_RotationController : OSKARObjectControllerBase
    {
        [SerializeField] private Transform childs;

        [SerializeField] private float rotateSpeed = 5;
        private float goToRotateSpeed = 1;
        private float currentRotateSpeed = 0;

        private bool swing;
        [SerializeField] private float swingSpeed = 0.01f;
        [SerializeField] private float maxSwing = 180;

        private void Awake()
        {
            this.fieldNames = new string[] { "rotate", "swing" };
        }

        public override void DataReceived_Bool(string fieldName, bool data)
        {
            if(fieldName == "rotate")
            {
                goToRotateSpeed = data ? rotateSpeed : 0;
            }
            if (fieldName == "swing")
            {
                swing = data;
            }
        }

        private void Update()
        {
            currentRotateSpeed = Mathf.Lerp(currentRotateSpeed, goToRotateSpeed, Time.deltaTime * 5);

            if (!swing)
            {
                if (currentRotateSpeed > 0.001f)
                {
                    childs.Rotate(Vector3.up, currentRotateSpeed * Time.deltaTime);
                }
            }
            if (swing)
            {
                childs.localRotation = Quaternion.Euler(Vector3.up * ((Mathf.Sin(Time.time * swingSpeed) * maxSwing)));
            }
        }
    }
}
