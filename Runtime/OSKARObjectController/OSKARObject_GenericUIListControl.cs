using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;

namespace rayd.oskar.content
{
    public class OSKARObject_GenericUIListControl : OSKARObjectController, IObjectInOutReceiver
    {
        [SerializeField] private TextMeshProUGUI hlLabel;
        [SerializeField] private TextMeshProUGUI slLabel;

        private OSKARObject_GenericUIListLine[] lines;

        private int itemsPerPage = int.MaxValue;
        private string content;
        private int page;

        private bool inTransition;
        [SerializeField] private float inAniTimeDelyy = 0.05f;
        [SerializeField] private float outAniTimeDelay = 0.03f;

        private bool visible;

        private void Awake()
        {
            lines = GetComponentsInChildren<OSKARObject_GenericUIListLine>();
            //lines.Reverse();
        }

        public override void DataReceived(OSKARObject_SetupData setupData)
        {

            base.DataReceived(setupData);
            foreach (var line in lines)
            {
                line.SetData(setupData);
            }
        }

        public override void DataReceived_Int(string fieldName, int data)
        {
            if (fieldName == "itemsPerPage")
            {
                if (data != itemsPerPage)
                {
                    ChangeItemsPerPage(data);
                }
            }
            if (fieldName == "page")
            {
                SetPage(data);
            }
        }


        public override void DataReceived_String(string fieldName, string data)
        {
            if (fieldName == "content")
            {
                if (content != data)
                {
                    if (!inTransition)
                    {
                        content = data;
                        int tempNewPage = page;
                        //Set page to max value to trigger change
                        page = int.MaxValue;
                        SetPage(tempNewPage);
                    }
                }
            }


            if (fieldName == "headline")
            {
                hlLabel.text = data;
            }

            if (fieldName == "subline")
            {
                slLabel.text = data;
            }
        }


        public override void DataReceived_Enum_Radio(string fieldName, int data)
        {
            if (fieldName == "page")
            {
                SetPage(data);
            }
        }

        private void ChangeItemsPerPage(int items)
        {
            items = Mathf.Clamp(items, 1, lines.Length);
            itemsPerPage = items;
            for (int i = 0; i < lines.Length; i++)
            {
                lines[i].gameObject.SetActive(i < items);
                lines[i].Reset();
            }
        }

        private void SetPage(int newPage)
        {
            //Debug.Log("Set page " + newPage + "  " + page+" , in transition: "+inTransition);
            if (newPage != page && !inTransition)
            {
                page = newPage;

                if (visible)
                {
                    StartCoroutine(ChangeRoutine());
                }
                else
                {

                    for (int i = 0; i < lines.Length; i++)
                    {
                        SetLine(i, GetLineContent(i));
                    }
                }
            }
            else
            {
               // Debug.Log("No new Page or in transition");
            }
        }

        private string GetLineContent(int lineIndex)
        {
            string[] lineData = content.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            int start = page * itemsPerPage;
            int dataIndex = lineIndex + start;
            if (lineIndex < itemsPerPage && dataIndex < lineData.Length)
            {
                //Debug.Log("Data index " + dataIndex+" lin index: "+lineIndex+" start "+start+" page "+page+" items per page "+itemsPerPage);
                return lineData[dataIndex];
            }
            return string.Empty;
        }

        private void SetLine(int line, string text)
        {
            lines[line].SetText(text);
            //Debug.Log("Set line: "+line+" text: "+text+" , global lines: "+lines.Length);
        }

        private IEnumerator ChangeRoutine()
        {
            ChangeStart();
            inTransition = true;
            for (int i = 0; i < lines.Length; i++)
            {
                lines[i].ChangeAni(GetLineContent(i));
                if (i < itemsPerPage)
                {
                    yield return new WaitForSeconds(inAniTimeDelyy);
                }
            }
            inTransition = false;
            ChangeDone();
        }

        protected virtual void ChangeStart()
        {

        }
        protected virtual void ChangeDone()
        {

        }

        public void ChangeInOut(OSKARObject_InOutController.INOUTState state)
        {
            bool shouldShow = state != OSKARObject_InOutController.INOUTState.OUT;
            if (!inTransition)
            {
                foreach (var line in lines)
                {
                    if (state == OSKARObject_InOutController.INOUTState.IN)
                    {
                        line.Reset();
                    }
                }

                visible = shouldShow;
                StartCoroutine(InOutRoutine(shouldShow));
            }
        }

        private IEnumerator InOutRoutine(bool show)
        {
            inTransition = true;
            yield return null;
            if (show)
            {
                for (int i = 0; i < lines.Length; i++)
                {
                    lines[i].Show(show);
                    if (i < itemsPerPage)
                    {
                        yield return new WaitForSeconds(inAniTimeDelyy);
                    }
                }
            }
            else
            {
                for (int i = lines.Length - 1; i >= 0; i--)
                {
                    lines[i].Show(show);
                    if (i < itemsPerPage)
                    {
                        yield return new WaitForSeconds(outAniTimeDelay);
                    }
                }
            }

            inTransition = false;
        }
    }
}