using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace rayd.oskar.content
{
    public class OSKARObjectController : OSKARObjectControllerBase
    {
        [SerializeField] private OSKARObjectData_ScriptableObject objectData;
        public OSKARObjectData_ScriptableObject ObjectData { get { return objectData; } }

        public override void DataReceived(OSKARObject_SetupData setupData)
        {
            //Debug.Log("Received Setup Data "+setupData.GetFieldData("show").value);

            base.DataReceived(setupData);
            foreach (var o in objectData.controlOptions)
            {
                //Debug.Log("Check data " + o.controlName+" "+setupData.GetFieldData(o.controlName).value);
                CheckData(setupData, o.controlName);
            }
        }

    }
}
