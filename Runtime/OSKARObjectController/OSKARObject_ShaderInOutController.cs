using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace rayd.oskar.content
{
    public class ShaderInOutReceiver : MonoBehaviour, IObjectInOutReceiver
    {
        [SerializeField] private Renderer[] fadeRenderer;
        private float goTo;
        private float start;
        private float a;
        private float t = 1;
        [SerializeField] private float speed = 1;

        public void ChangeInOut(OSKARObject_InOutController.INOUTState state)
        {
            start = a;
            goTo = state == OSKARObject_InOutController.INOUTState.OUT ? 0 : 1;
            t = 1 - t;
        }

        void Update()
        {
            if (t < 1)
            {
                t += Time.deltaTime * speed;
                t = Mathf.Clamp(t, 0, 1);
                a = Mathf.Lerp(start, goTo, t);
                Apply();
            }
        }

        private void Apply()
        {
            foreach (Renderer r in fadeRenderer)
            {
                r.material.SetFloat("_Alpha", a);
                r.enabled = a != 0;
            }
        }
    }
}