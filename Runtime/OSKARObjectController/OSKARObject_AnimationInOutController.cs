using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEditor;
using UnityEngine;

namespace rayd.oskar.content
{
    public class OSKARObject_AnimationInOutController : MonoBehaviour, IObjectInOutReceiver
    {
        [Header("Apply an animator with nodes 'show' (bool) and 'reset' (trigger)")]
        [Space(10)]
        [SerializeField]private Animator ani;
        private OSKARObject_InOutController.INOUTState currentState;

        private bool hasInstantOut;
        private bool hasReset;
        private bool hasShowInt;
        private bool hasShowBool;

        void Awake()
        {
            if (ani == null)
            {
                ani = GetComponent<Animator>();
            }
            if (ani == null)
            {
                Debug.LogWarning("Could not find Ani for INOUT Behaviour on object " + gameObject.name);
                return;
            }

            //cache the animation controller paramters
            hasInstantOut = HasParameter("instantOut", AnimatorControllerParameterType.Trigger, ani);
            hasReset = HasParameter("reset", AnimatorControllerParameterType.Trigger, ani);
            hasShowInt = HasParameter("show", AnimatorControllerParameterType.Int, ani);
            hasShowBool = HasParameter("show", AnimatorControllerParameterType.Bool, ani);

            //Reset on start
            ResetAni();
        }

        public void ChangeInOut(OSKARObject_InOutController.INOUTState state)
        {
            if (ani != null)
            {
                ShowAni(state != OSKARObject_InOutController.INOUTState.OUT);
                if (state == OSKARObject_InOutController.INOUTState.OUT && currentState != OSKARObject_InOutController.INOUTState.IN)
                {
                    //preview out
                    //ani.SetTrigger("instantOut");
                }
                if (state == OSKARObject_InOutController.INOUTState.IN)
                {
                    ResetAni();
                }

                currentState = state;
            }
        }

        private void ResetAni()
        {
            if(hasInstantOut)
            {
                ani.SetTrigger("instantOut");
            }
            if (hasReset)
            {
                ani.SetTrigger("reset");
            }
        }

        private void ShowAni(bool show)
        {
            if(hasShowInt)
            {
                ani.SetInteger("show", show ? 0 : 1);
            }
            if (hasShowBool)
            {
                ani.SetBool("show", show);
            }
        }

        public static bool HasParameter(string paramName, AnimatorControllerParameterType type, Animator animator)
        {
            foreach (AnimatorControllerParameter param in animator.parameters)
            {
                if (param.name == paramName && param.type == type)
                    return true;
            }
            return false;
        }
    }
}
