using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
namespace rayd.oskar.content
{
    public class OSKARObject_VideoPlaybackController : MonoBehaviour, IObjectInOutReceiver, IOskarDataReceiver
    {

        [SerializeField] private VideoPlayer video;
        [Tooltip("Let the video play this long after the out comes, for any out animation/fading to be finished")]
        [SerializeField] private float outStopTime = 1;
        private OSKARObject_InOutController.INOUTState currentState = OSKARObject_InOutController.INOUTState.PREVIEW;

        private bool autoplay = true;
        private bool loop = true;
        private bool shouldPlay = true;

        private Texture2D thumbnail;
        [SerializeField] private RawImage videoImg;
        [SerializeField] private Renderer videoRenderer;

        public void DataReceived_Bool(string fieldName, bool data)
        {
            switch(fieldName)
            {
                case "autoplay":
                    autoplay = data;
                    break;
                case "loop":
                    loop = data;
                    video.isLooping = loop;
                    break;
                case "play":
                    shouldPlay = data;
                    if (!autoplay)
                    {
                        if (shouldPlay)
                        {
                            video.Play();
                            SetTexture();
                        }
                        else
                        {
                            video.Pause();
                            SetTexture();
                        }
                    }
                    break;
            }
        }
        public virtual void DataReceived_Texture(string fieldName, Texture2D texture)
        {
            if(fieldName == "thumbnail")
            {
                thumbnail = texture;
                SetTexture();
            }
        }
        private void SetTexture()
        {
            if(thumbnail != null)
            {
                bool showRT = video.isPlaying && (ulong)video.frame != video.frameCount-1;
                if(videoImg != null)
                {
                    videoImg.texture = showRT ? video.targetTexture:thumbnail;
                }
                if(videoRenderer != null)
                {
                    videoRenderer.material.mainTexture = showRT ? video.targetTexture : thumbnail;
                }
            }
        }
        public void ChangeInOut(OSKARObject_InOutController.INOUTState state)
        {

            if (state != currentState)
            {
                //Debug.Log("Change video State! " + state);
                if (state == OSKARObject_InOutController.INOUTState.OUT)
                {
                    StartCoroutine("PauseAfterTime");
                    // Debug.Log("Stop");
                }
                else
                {
                    StopAllCoroutines();
                    if (autoplay)
                    {
                        StartCoroutine(PlayAfterTime());
                        //Debug.Log("Play!");
                    }
                    else
                    {
                        video.frame = 0;
                    }
                }
                currentState = state;
            }
            
        }

        void Update()
        {
            if (video.isPlaying && !loop)
            {
                if((ulong)video.frame == video.frameCount - 1)
                {
                    SetTexture();
                }
            }
        }

        private IEnumerator PauseAfterTime()
        {
            yield return new WaitForSeconds(outStopTime);
            video.Pause();
            SetTexture();
        }

        private IEnumerator PlayAfterTime()
        {
            yield return new WaitForSeconds(0.1f);
            video.frame = 0;
            video.Play();
            //Debug.Log(video.isPlaying + " ___ " + video.frame);
            yield return new WaitForSeconds(0.1f);
            SetTexture();
        }
    }

}
