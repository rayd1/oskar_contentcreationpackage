using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace rayd.oskar.content
{
    /// <summary>
    /// This class is the base for an ARController - this receives Data and checks if they need to be applied.
    /// </summary>
    public class OSKARObjectControllerBase : MonoBehaviour
    {
        private IMediaProvider mediaProvider;

        protected string[] fieldNames;
        private List<IOskarDataReceiver> receivers = new List<IOskarDataReceiver>();

#if UNITY_EDITOR
        //Workaround for editor testing to init the object. TODO: Maybe call this by the editor tester script?
        private void Start()
        {
            //Init(null);      
        }
#endif

        //This should be called on init, to get images
        public void Init(IMediaProvider mediaProvider)
        {
            this.mediaProvider = mediaProvider;
            //get all other receivers
            receivers = new List<IOskarDataReceiver>();       
            foreach (var child in GetComponentsInChildren<MonoBehaviour>())
            {
                if(child == this)
                {
                    continue;
                }
                if(child.GetComponent<IOskarDataReceiver>()!=null)
                {
                    receivers.Add(child.GetComponent<IOskarDataReceiver>());
                }
            }
            //Debug.Log("Found " + receivers.Count + " receivers");
        }


        //This gets called whenever the data is changed!
        public virtual void DataReceived(OSKARObject_SetupData setupData)
        {
            if (fieldNames != null)
            {
                foreach (string fieldName in fieldNames)
                {
                    CheckData(setupData, fieldName);
                }
            }
        }

        protected virtual void CheckData(OSKARObject_SetupData setupData, string fieldName)
        {
            var data = setupData.GetFieldData(fieldName);
            if (data != null)
            {
                //get Value
                ProcessData(fieldName, data.value, data.controlType);
            }
        }

        protected virtual void ProcessData(string fieldName, string dataString, DataFieldBlueprint.ControlTypes controlType)
        {
           // Debug.Log("Processing Data "+dataString);
            switch (controlType)
            {
                case DataFieldBlueprint.ControlTypes.STRING:
                    DataReceived_String(fieldName, dataString);
                    break;
                case DataFieldBlueprint.ControlTypes.BOOL:
                case DataFieldBlueprint.ControlTypes.TRIGGER:
                    bool b = false;
                    if (bool.TryParse(dataString, out b))
                    {
                        if (controlType == DataFieldBlueprint.ControlTypes.BOOL)
                        {
                            DataReceived_Bool(fieldName, b);
                        }
                        else
                        {
                            if (b == true)
                            {
                                DataReceived_Trigger(fieldName);
                            }
                        }
                    }
                    break;
                case DataFieldBlueprint.ControlTypes.INT:
                case DataFieldBlueprint.ControlTypes.INTRANGE:
                case DataFieldBlueprint.ControlTypes.ENUM:
                case DataFieldBlueprint.ControlTypes.RADIO:
                    int i = 0;
                    if (int.TryParse(dataString, out i))
                    {
                        if (controlType == DataFieldBlueprint.ControlTypes.ENUM || controlType == DataFieldBlueprint.ControlTypes.RADIO)
                        {
                            DataReceived_Enum_Radio(fieldName, i);
                        }
                        else
                        {
                            DataReceived_Int(fieldName, i);
                        }
                    }
                    break;
                case DataFieldBlueprint.ControlTypes.FLOAT:
                case DataFieldBlueprint.ControlTypes.FLOATRANGE:
                    float f = 0;
                    if (float.TryParse(dataString, out f))
                    {
                        DataReceived_Float(fieldName, f);
                    }
                    break;
                case DataFieldBlueprint.ControlTypes.DBIMAGE:
                    if(mediaProvider != null)
                    {
                        DataReceived_Texture(fieldName, mediaProvider.GetImage(dataString));
                    }
                    break;
                case DataFieldBlueprint.ControlTypes.VIDEO:
                    DataReceived_Video(fieldName,mediaProvider.GetVideoURL(dataString));
                    break;
            }

        }

        public virtual void DataReceived_String(string fieldName, string data)
        {
            foreach(IOskarDataReceiver r in receivers)
            {
                r.DataReceived_String(fieldName, data);
            }
        }
        public virtual void DataReceived_Bool(string fieldName, bool data)
        {
            foreach (IOskarDataReceiver r in receivers)
            {
                r.DataReceived_Bool(fieldName, data);
            }
        }
        public virtual void DataReceived_Int(string fieldName, int data)
        {
            foreach (IOskarDataReceiver r in receivers)
            {
                r.DataReceived_Int(fieldName, data);
            }
        }
        public virtual void DataReceived_Enum_Radio(string fieldName, int data)
        {
            foreach (IOskarDataReceiver r in receivers)
            {
                r.DataReceived_Enum_Radio(fieldName, data);
            }
        }
        public virtual void DataReceived_Float(string fieldName, float data)
        {
            foreach (IOskarDataReceiver r in receivers)
            {
                r.DataReceived_Float(fieldName, data);
            }
        }
        public virtual void DataReceived_Trigger(string fieldName)
        {
            foreach (IOskarDataReceiver r in receivers)
            {
                r.DataReceived_Trigger(fieldName);
            }
        }
        public virtual void DataReceived_Texture(string fieldName, Texture2D texture)
        {
            foreach (IOskarDataReceiver r in receivers)
            {
                r.DataReceived_Texture(fieldName, texture);
            }
        }

        public virtual void DataReceived_Video(string fieldName, string videoName)
        {
            foreach (IOskarDataReceiver r in receivers)
            {
                r.DataReceived_Video(fieldName, videoName);
            }
        }
    }
}
