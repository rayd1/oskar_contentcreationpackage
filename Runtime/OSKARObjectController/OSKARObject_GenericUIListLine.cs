using UnityEngine;
using UnityEngine.UI;

namespace rayd.oskar.content
{
    public class OSKARObject_GenericUIListLine : MonoBehaviour
    {

        [SerializeField] private Animator ani;
        [SerializeField] private GameObject[] hideOnEmptyElements;
        [SerializeField] private LayoutElement layoutElement;

        private string changeCacheText;
        private bool changing;

        protected bool empty;

        protected virtual void Awake()
        {

        }

        public void Show(bool show)
        {
            ani.SetBool("show", show);
        }

        public void Reset()
        {
            ani.SetBool("show", false);
            ani.SetTrigger("reset");
        }

        public virtual void SetText(string lineText)
        {
            empty = string.IsNullOrEmpty(lineText);

            foreach (GameObject o in hideOnEmptyElements)
            {
                o.SetActive(!empty);
            }
            Show(!empty);

        }

        public virtual void SetData(OSKARObject_SetupData data)
        {

        }


        public void ChangeAni(string newText)
        {
            changing = true;
            changeCacheText = newText;
            ani.SetTrigger("change");
        }

        public void ChangeAniEnd()
        {
            if (changing)
            {
                SetText(changeCacheText);
                changeCacheText = null;
                changing = false;
            }
        }

        public void ShowAniEnd()
        {
            if (layoutElement != null)
            {
                layoutElement.ignoreLayout = empty;
            }
        }
    }
}
