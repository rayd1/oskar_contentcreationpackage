using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace rayd.oskar.content
{
    public class OSKARObject_BillboardController : OSKARObjectControllerBase
    {
        private bool billboardEnabled;
        private Transform cam;

        private void Awake()
        {
            if(Camera.main == null)
            {
                //Disable!
                Debug.LogWarning("Could not find Camera, disabling");
                enabled = false;
                return;
            }
            cam = Camera.main.transform;
            this.fieldNames = new string[] { "billboard" };

        }

        public override void DataReceived_Bool(string fieldName, bool data)
        {
            if (fieldName == "billboard")
            {
                billboardEnabled = data;

                if (!billboardEnabled)
                {
                    transform.localRotation = Quaternion.identity;
                }
            }

        }

        private void Update()
        {
            if (billboardEnabled)
            {
                transform.LookAt(cam, Vector3.up);
                transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y + 180, 0);

                /*
                transform.LookAt(cam, Vector3.up);
                Vector3 eulerAngles = transform.eulerAngles;
                eulerAngles.Scale(axis);
                transform.rotation = Quaternion.Euler(eulerAngles);
                */
            }
        }
    }
}