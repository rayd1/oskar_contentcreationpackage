using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace rayd.oskar.content
{
    public class OSKARObject_InOutController : OSKARObjectControllerBase
    {
        private int normalLayer = 0;
        private int previewLayer = 31;
        [SerializeField] private GameObject childs;

        private IObjectInOutReceiver[] inOutReceivers;
        private INOUTState currentState;

        public enum INOUTState
        {
            IN,
            OUT,
            PREVIEW
        }


        private void Awake()
        {
            this.fieldNames = new string[] { "show" };


            inOutReceivers = GetComponentsInChildren<IObjectInOutReceiver>();
        }

        public override void DataReceived_Enum_Radio(string fieldName, int data)
        {
            INOUTState state = (INOUTState)data;
            if (state != currentState)
            {
                gameObject.SetLayerRecursively(state == INOUTState.PREVIEW ? previewLayer : normalLayer);

                childs.SetLayerRecursively(state == INOUTState.PREVIEW ? previewLayer : normalLayer);

                foreach (var io in inOutReceivers)
                {
                    io.ChangeInOut(state);
                }
                currentState = state;

            }
        }
    }
}
