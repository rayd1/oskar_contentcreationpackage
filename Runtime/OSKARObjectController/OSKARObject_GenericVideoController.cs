using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System.IO;

namespace rayd.oskar.content
{
    public class OSKARObject_GenericVideoController : OSKARObjectController
    {
        [SerializeField] private VideoPlayer player;
        [SerializeField] private RawImage videoPlane;
        [SerializeField] private Canvas canvas;

        [Space(10)]
        [SerializeField] private TMPro.TextMeshProUGUI sublineLabel;
        [SerializeField] private string subLineFieldName = "subline";
        [SerializeField] private GameObject sublineObject;

        private RenderTexture rt;
        //Check if a rendertexture is set via the inspector - if so, do not update dynamically
        private bool legacyMode = false;

        private void Awake()
        {
            player.playOnAwake = false;
            legacyMode = player.targetTexture != null;

        }

        public override void DataReceived_String(string fieldName, string data)
        {
            if(sublineLabel != null)
            {
                if(fieldName == subLineFieldName)
                {
                    sublineLabel.text = data;
                    if(sublineObject != null)
                    {
                        sublineObject.SetActive(!string.IsNullOrEmpty(data));
                    }
                }
            }
        }
        public override void DataReceived_Video(string fieldName, string videoURL)
        {
            
            if (!string.IsNullOrEmpty(videoURL))
            {
                if(player.url != "file://" + videoURL)
                {
                    player.url = "file://" + videoURL;
                    //Debug.LogWarning("Player URL: "+player.url);
                    if (!legacyMode)
                    {
                        player.prepareCompleted += Player_prepareCompleted;
                        player.Prepare();
                    }
                }
            }
            else
            {
                player.url = "";
            }
        }

        private void Player_prepareCompleted(VideoPlayer source)
        {
            CheckRT();
            player.prepareCompleted -= Player_prepareCompleted;
        }

        private void CheckRT()
        {
            if (legacyMode)
            {
                return;
            }

            Vector2Int size = new Vector2Int((int)player.width, (int)player.height);
            //Debug.Log("Video size " + size);
            if(rt  == null || size.x != rt.width|| size.y != rt.height)
            {
                if(rt != null)
                {
                    rt.Release();
                    player.targetTexture = null;
                    rt = null;

                }
                //create new RT
                rt = new RenderTexture(size.x, size.y, 16, RenderTextureFormat.Default);
                videoPlane.texture = rt;

                //apply
                player.targetTexture = rt;
                if(videoPlane != null)
                {
                    videoPlane.texture = rt;
                }

                //rescale Canvas
                canvas.GetComponent<RectTransform>().sizeDelta = size;
            }
        }

        private void OnDisable()
        {
            player.Stop();
            if(rt!= null)
            {
                rt.Release();
                rt = null;
            }
        }
    }
}
