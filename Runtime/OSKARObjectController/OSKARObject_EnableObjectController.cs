using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace rayd.oskar.content
{
    public class OSKARObject_EnableObjectController : OSKARObjectControllerBase
    {
        [SerializeField] private string[] dataFieldNames;
        private void Awake()
        {
            this.fieldNames = dataFieldNames;
        }

        [SerializeField] private GameObject toEnable;
        public override void DataReceived_Bool(string fieldName, bool data)
        {
            toEnable.SetActive(data);
        }
    }
}