using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    [SerializeField] private Vector3 axis = new Vector3(0, 1, 0);
    private Transform cam;
    private void Awake() {
        cam = Camera.main.transform;
    }

    private void LateUpdate() {

        if (cam != null) {
            transform.rotation = cam.transform.rotation;
        }
    }

}
