using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace rayd.oskar.content
{
    public class WorldUIShaderOffsetSetter : MonoBehaviour
    {
        [SerializeField] private string shaderPropPosition = "_GlobalPos";
        [SerializeField] private string shaderPropScale = "_ScaleFactor";

        Material[] mats;

        private Vector3 startScale;

        private void Awake()
        {
            List<Material> m = new List<Material>();
            foreach(Graphic g in GetComponentsInChildren<Graphic>())
            {
                if(g.material != null && g.material.HasVector(shaderPropPosition))
                {
                    m.Add(g.material);
                }
            }
            mats = m.ToArray();
        }

        private void Start()
        {
            startScale = transform.localScale;
        }

        void Update()
        {
            foreach(Material m in mats)
            {
                m.SetVector(shaderPropPosition, transform.position);
                m.SetVector(shaderPropScale, Vector3.one*(transform.localScale.x/startScale.x));
            }
        }

    }
}
