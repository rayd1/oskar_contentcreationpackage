﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class UIHelper {

    public static void Show(this CanvasGroup cGroup, bool show, bool switchRaycastBlocking = true) {
        if(cGroup == null)
        {
            return;
        }
        cGroup.alpha = show ? 1f : 0f;
        if(switchRaycastBlocking)
            cGroup.blocksRaycasts = show;
    }

}
