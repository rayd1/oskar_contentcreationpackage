using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace rayd.oskar.content
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Sets the layer of this GameObject and all of its descendants.
        /// </summary>
        /// <param name="gameObject">The GameObject at the root of the hierarchy to be modified.</param>
        /// <param name="layer">The layer to recursively assign GameObjects to.</param>
        public static void SetLayerRecursively(this GameObject gameObject, int layer)
        {
            gameObject.layer = layer;
            foreach (Transform child in gameObject.transform)
            {
                child.gameObject.SetLayerRecursively(layer);
            }
        }
    }
}
