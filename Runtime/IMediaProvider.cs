using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace rayd.oskar.content
{
    public interface IMediaProvider
    {
        public Texture2D GetImage(string id);
        public string GetVideoURL(string id);
    }
}
