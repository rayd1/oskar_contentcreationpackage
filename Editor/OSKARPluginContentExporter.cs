using rayd.oskar.content;
using System;
using System.IO;
using System.Reflection;
using Unity.SharpZipLib;
using Unity.SharpZipLib.Utils;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using System.Collections;
using Newtonsoft.Json.Linq;
using System.Text;
using Newtonsoft.Json;

public class OSKARPluginContentExporter : EditorWindow
{
    OSKARPluginExportSetting settings;

    [MenuItem("OSKAR/OskarContentExporter")]
    public static void ShowEditorWindow()
    {
        OSKARPluginContentExporter wnd = GetWindow<OSKARPluginContentExporter>();
        wnd.titleContent = new GUIContent("OskarContentExporter");
    }

    private void Awake()
    {
        LoadSettings();
        CreateGUI();
    }


    private void LoadSettings()
    {
        OSKARPluginExportSetting myInstance = (OSKARPluginExportSetting)Resources.Load("OSKARContentCreationPlugin/oskar_exportsettings.asset") as OSKARPluginExportSetting;
        if (myInstance == null)
        {
            myInstance = CreateInstance<OSKARPluginExportSetting>();
            if (!AssetDatabase.IsValidFolder("Assets/Resources"))
            {
                AssetDatabase.CreateFolder("Assets", "Resources");
            }
            if (!AssetDatabase.IsValidFolder("Assets/Resources/OSKARContentCreationPlugin"))
            {
                AssetDatabase.CreateFolder("Assets/Resources", "OSKARContentCreationPlugin");
            }
            AssetDatabase.CreateAsset(myInstance, "Assets/Resources/OSKARContentCreationPlugin/oskar_exportsettings.asset");
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
        settings = myInstance;
    }
    public void CreateGUI()
    {
        rootVisualElement.Clear();
        rootVisualElement.style.alignItems = Align.Center;

        var introductionText = new Label("This lets you export all OSKAR Objects to upload them to the CMS and add them to the app. Put all your Assets in the 'OSKAR Content' folder, or choose the folder in this editor window. If you hit Exort Packages, this tool will seach for all OSKAR_ObjectData Scriptable Objects, and will export everyone which has a prefab assigned.");
        introductionText.style.whiteSpace = WhiteSpace.Normal;
        introductionText.style.marginBottom = 10;
        introductionText.style.marginTop = 10;
        introductionText.style.marginLeft = 10;
        introductionText.style.marginRight = 10;
        rootVisualElement.Add(introductionText);

        var folderButton = new Button { text = "Assets Path: " + settings.path.ToString() };
        folderButton.style.whiteSpace = WhiteSpace.Normal;
        folderButton.RegisterCallback<MouseUpEvent>((e) => {
            SetFolder();
        });
        rootVisualElement.Add(folderButton);

        //Check if folder is set
        if (string.IsNullOrEmpty(settings.path))
        {
            var errorLabel = new Label("You need to assign a folder with all your content for the OSKAR Package fist!");
            errorLabel.style.whiteSpace = WhiteSpace.Normal;
            rootVisualElement.Add(errorLabel);
            return;
        }

        //Export Button
        var exportButton = new Button { text = "Export Packages" };
        exportButton.style.width = 100;
        exportButton.RegisterCallback<MouseUpEvent>((e) => {
            ExportPackages();
        });
        rootVisualElement.Add(exportButton);

    }

    private void SetFolder()
    {
        string path = "none";
        TryGetActiveFolderPath(out path);
        if (!string.IsNullOrEmpty(path))
        {
            settings.path = path;
            Refresh();
        }
    }

    private void ExportPackages()
    {
        //get all OSKAR Object Data
        string[] guids = AssetDatabase.FindAssets("t:OSKARObjectData_ScriptableObject", new[] { settings.path });
        string tempPath = Path.Combine(GetPath(), "oskarDataTemp");


        foreach (string guid in guids)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(guid);
            UnityEngine.Object asset = AssetDatabase.LoadAssetAtPath(assetPath, typeof(OSKARObjectData_ScriptableObject));

            if (asset != null)
            {
                OSKARObjectData_ScriptableObject oskarObject = asset as OSKARObjectData_ScriptableObject;
                if (oskarObject.renderPrefab != null)
                {
                    if (!Directory.Exists(tempPath))
                    {
                        Directory.CreateDirectory(tempPath);
                    }

                    //export package to temp path
                    AssetDatabase.ExportPackage(assetPath, Path.Combine(tempPath,oskarObject.name + ".unitypackage"), ExportPackageOptions.IncludeDependencies);
                    //Debug.Log("Exported " + oskarObject.name);
                    //create oskardata and export
                    OSKARObjectDataBlueprint bp = OSKARObjectDataBlueprint.GetData(oskarObject);
                    WriteSaveFile_Json(oskarObject.name + ".oskardata", bp, tempPath);

                    //create Meta file
                    OskarPackageMetadata meta = new OskarPackageMetadata();
                    meta.packageName = oskarObject.name;
                    meta.unityVersion = Application.unityVersion;
                    meta.creationTimestamp = DateTime.Now.ToString();
                    meta.OSKARPackageVersion = GetPackageVersion("com.rayd.oskar.content");

                    var dependencies = AssetDatabase.GetDependencies(assetPath);
                    foreach (var dependency in dependencies)
                    {
                        if (dependency.StartsWith(settings.path))
                        {
                            if (dependency.EndsWith(".cs"))
                            {
                                meta.needsCompilation = true;
                                break;
                            }
                            if (dependency.EndsWith(".shader"))
                            {
                                meta.needsCompilation = true;
                                break;
                            }
                        }
                    }

                    WriteSaveFile_Json("pkginfo.json",meta, tempPath);

                    //zip all
                    string zipFolderPath = Path.Combine(GetPath(), "DataExport");
                    
                    if (!Directory.Exists(zipFolderPath))
                    {
                        Directory.CreateDirectory(zipFolderPath);
                    }
                    ZipUtility.CompressFolderToZip(Path.Combine(zipFolderPath, oskarObject.name )+ ".oskarpkg", "", tempPath);

                    //delete temp
                    Directory.Delete(tempPath, true);
                }
            }
        }


    }

    //Gets the package Version for Metadataa
    private string GetPackageVersion(string packageName)
    {
        string manifestPath = Path.Combine(Application.dataPath, "../Packages/manifest.json");
        if (File.Exists(manifestPath))
        {
            string content = File.ReadAllText(manifestPath);
            //Debug.Log(content);
            JObject manifest = JObject.Parse(content);
            if (manifest.TryGetValue("dependencies", out JToken dependencies)){
                foreach (var dependency in dependencies.Children<JProperty>())
                {
                    if(dependency.Name == packageName)
                    {
                        return dependency.Value.ToString();
                    }
                }
            }
        }
        return "null";
    }

    private void Refresh()
    {
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        CreateGUI();
    }

    // Define this function somewhere in your editor class to make a shortcut to said hidden function
    private static bool TryGetActiveFolderPath(out string path)
    {
        var _tryGetActiveFolderPath = typeof(ProjectWindowUtil).GetMethod("TryGetActiveFolderPath", BindingFlags.Static | BindingFlags.NonPublic);

        object[] args = new object[] { null };
        bool found = (bool)_tryGetActiveFolderPath.Invoke(null, args);
        path = (string)args[0];

        return found;
    }

    [System.Serializable]
    public class OskarPackageMetadata
    {
        public string packageName;
        public string unityVersion;
        public string OSKARPackageVersion;
        public bool needsCompilation;
        public string creationTimestamp;
    }

    //Helper functions
    public static void WriteSaveFile_Json(string filename, object obj, string _directory)
    {
        string combinedPath = Path.Combine(GetPath(), Path.Combine(_directory, filename));
        string subDirectory = Path.GetDirectoryName(combinedPath);

        if (!Directory.Exists(subDirectory))
        {
            Directory.CreateDirectory(subDirectory);
        }

        JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto, Formatting = Newtonsoft.Json.Formatting.Indented };
        string jsonString = JsonConvert.SerializeObject(obj, settings);

        File.WriteAllText(combinedPath, jsonString, Encoding.UTF8);
    }

    	public static string GetPath() {
		    string dataPath = Application.dataPath;

		    if (Application.isEditor) {
			    dataPath = dataPath.Remove(dataPath.Length - 7);
		    } else {
    #if UNITY_ANDROID || UNITY_IOS
			    dataPath = Application.persistentDataPath;
    #else
			    dataPath = Directory.GetParent(dataPath).ToString();		
    #endif
		    }
            return (dataPath + "/");
	}

}
