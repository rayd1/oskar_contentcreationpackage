using Newtonsoft.Json;
using rayd.oskar.content;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class OskarContentEditorTester : EditorWindow
{
    //[SerializeField]
    //private StyleSheet m_StyleSheet = default;

    OSKARObjectController toTestObject;
    OSKARObject_SetupData testData;
    OSKARObject_SetupdataCollection dataCollection;

    [MenuItem("OSKAR/OskarContentEditorTester")]
    public static void ShowEditorWindow()
    {
        OskarContentEditorTester wnd = GetWindow<OskarContentEditorTester>();
        wnd.titleContent = new GUIContent("OskarContentEditorTester");
    }

    private void Awake()
    {

    }

    private void OnEnable()
    {
        dataCollection = new OSKARObject_SetupdataCollection();
        //Try to load data
        Load();
        GetCurrentSelection();
        CreateGUI();
    }


    void InitObjects()
    {
        //find all OskarObjects and init
        var oskarObjects = FindObjectsByType<OSKARObjectControllerBase>(FindObjectsSortMode.None);
        foreach(var o in oskarObjects)
        {
            o.Init(null);
        }
    }

    public void CreateGUI()
    {
        InitObjects();

        rootVisualElement.Clear();
        //reset utton
        var resetButton = new Button { text = "Clear Data" };
        resetButton.style.width = 100;
        resetButton.style.alignSelf = Align.FlexEnd;

        //reapply utton
        var button = new Button { text = "Re Apply" };
        button.style.width = 100;
        button.style.alignSelf = Align.FlexEnd;
        button.RegisterCallback<MouseUpEvent>((e) => {
            ApplyData();
        });
        rootVisualElement.Add(button);

        resetButton.RegisterCallback<MouseUpEvent>((e) => {
            ClearData();
        });
        rootVisualElement.Add(resetButton);

        if (toTestObject != null && toTestObject.ObjectData != null && testData != null)
        {
            OSKARObjectData_ScriptableObject dataBlueprint = toTestObject.ObjectData;
            //Build controls

            foreach (DataFieldBlueprint c in dataBlueprint.controlOptions)
            {
                var group = new Foldout { text = c.controlName };
                //group.style.paddingTop = 10;
                rootVisualElement.Add(group);

                switch (c.controlType)
                {
                    case DataFieldBlueprint.ControlTypes.STRING:
                        var textField = new TextField();
                        textField.value = testData.GetFieldData(c.controlName).value;
                        textField.RegisterCallback<ChangeEvent<string>>((e) =>
                        {
                            testData.GetFieldData(c.controlName).value = e.newValue;
                            ApplyData();
                        });
                        group.Add(textField);
                        break;
                    case DataFieldBlueprint.ControlTypes.BOOL:
                        var toggle = new Toggle();
                        toggle.value = bool.Parse(testData.GetFieldData(c.controlName).value);
                        toggle.RegisterCallback<ChangeEvent<bool>>((e) =>
                        {
                            testData.GetFieldData(c.controlName).value = e.newValue.ToString();
                            ApplyData();
                        });
                        group.Add(toggle);
                        break;
                    case DataFieldBlueprint.ControlTypes.RADIO:
                        int selected = int.Parse(testData.GetFieldData(c.controlName).value);
                        string[] options = c.additionalData.Split('\n');
                        var radiobuttongroup = new RadioButtonGroup("",options.ToList());
                        radiobuttongroup.value = selected;
                        radiobuttongroup.RegisterValueChangedCallback(e =>
                        {
                            testData.GetFieldData(c.controlName).value = e.newValue.ToString();
                            ApplyData();
                            //Debug.Log("Change value " + testData.GetFieldData(c.controlName).value.ToString());
                        });
                        group.Add(radiobuttongroup);
                        break;
                    case DataFieldBlueprint.ControlTypes.FLOAT:
                        var floatField = new FloatField();
                        floatField.value = float.Parse(testData.GetFieldData(c.controlName).value);
                        floatField.RegisterValueChangedCallback<float>((e) =>
                        {
                            testData.GetFieldData(c.controlName).value = e.newValue.ToString();
                        });
                        group.Add(floatField);
                        break;
                    case DataFieldBlueprint.ControlTypes.INT:
                        var intField = new FloatField();
                        intField.value = int.Parse(testData.GetFieldData(c.controlName).value);
                        intField.RegisterValueChangedCallback((e) =>
                        {
                            testData.GetFieldData(c.controlName).value = e.newValue.ToString();
                        });
                        group.Add(intField);
                        break;
                    case DataFieldBlueprint.ControlTypes.TRIGGER:
                        var triggerbutton = new Button();
                        triggerbutton.style.height = 20;
                        triggerbutton.RegisterCallback<MouseUpEvent>((e) =>
                        {
                            testData.GetFieldData(c.controlName).value = "1";
                        });
                        group.Add(triggerbutton);
                        break;
                    case DataFieldBlueprint.ControlTypes.INTRANGE:
                    case DataFieldBlueprint.ControlTypes.FLOATRANGE:
                        var parts = c.additionalData.Split('\n');
                        Vector2 minMaxValues = new Vector2(float.Parse(parts[0]), float.Parse(parts[1]));
                        var minmax = new Slider();
                        minmax.lowValue = minMaxValues.x;
                        minmax.highValue = minMaxValues.y;
                        minmax.RegisterValueChangedCallback((e) =>
                        {
                            float toSet = e.newValue;
                            if(c.controlType == DataFieldBlueprint.ControlTypes.INTRANGE)
                            {
                                toSet = (float)Mathf.RoundToInt(toSet);
                            }
                            testData.GetFieldData(c.controlName).value = toSet.ToString();
                        });
                        group.Add(minmax);
                        break;
                    case DataFieldBlueprint.ControlTypes.ENUM:
                        var dropdown = new DropdownField();
                        string[] dropdownoptions = c.additionalData.Split('\n');
                        foreach(string s in dropdownoptions)
                        {
                            dropdown.choices.Add(s);
                        }
                        dropdown.index = int.Parse(testData.GetFieldData(c.controlName).value);
                        dropdown.RegisterValueChangedCallback((e) =>
                        {
                            for(int i = 0;i< dropdown.choices.Count;i++)
                            {
                                if (dropdown.choices[i]  == e.newValue)
                                {
                                    testData.GetFieldData(c.controlName).value = i.ToString();
                                }
                            }
                        });
                        group.Add(dropdown);
                        break;
                    case DataFieldBlueprint.ControlTypes.DBIMAGE:
                        var imageField = new TextField();
                        imageField.value = testData.GetFieldData(c.controlName).value;
                        imageField.RegisterCallback<ChangeEvent<string>>((e) =>
                        {
                            testData.GetFieldData(c.controlName).value = e.newValue;
                            ApplyData();
                        });
                        group.Add(imageField);
                        break;
                }
            }

        }
        else
        {
            // GUILayout.Label("Select an object");
            VisualElement container = new VisualElement();
            container.style.marginLeft = 10;
            container.style.marginRight = 10;
            rootVisualElement.Add(container);
            var label = new Label("Select an Object with at least one OSKARObjectController on it.");
            label.style.unityTextAlign = TextAnchor.MiddleCenter;
            label.style.whiteSpace = WhiteSpace.Normal;
            container.Add(label);
        }
    }

    private void ApplyData()
    {
        foreach (var oskarcontroller in toTestObject.GetComponents<OSKARObjectControllerBase>())
        {
            oskarcontroller.DataReceived(testData);
        }
    }

    private void OnSelectionChange()
    {
        Save();
        GetCurrentSelection();
        CreateGUI();
    }

    private void GetCurrentSelection()
    {
        if (Selection.activeGameObject != null && Selection.activeGameObject.GetComponent<OSKARObjectController>() != null)
        {
            toTestObject = Selection.activeGameObject.GetComponent<OSKARObjectController>();
            //Check if data is in collection
            testData = GetCurrentData();
        }
    }

    private OSKARObject_SetupData GetCurrentData()
    {

        if (dataCollection.GetDataByID(toTestObject.ObjectData.name) == null)
        {
            OSKARObject_SetupData newData = new OSKARObject_SetupData();
            newData.objectID = toTestObject.ObjectData.name;
            //Initialize
            foreach (var dataField in toTestObject.ObjectData.controlOptions)
            {
                string defaultValue = string.IsNullOrEmpty(dataField.defaultValue) ? dataField.GetDefaultValue() : dataField.defaultValue;
                newData.InitFieldData(dataField.controlName, defaultValue, dataField.controlType);
            }
            //add 
            dataCollection.datas.Add(newData);

            Debug.Log($"Added new Data {newData.objectID} in Content Tester Window prefs!");

            //Save
            Save();
        }

        //Get Data from collection
        return dataCollection.GetDataByID(toTestObject.ObjectData.name);
    }

    private void Save()
    {
        JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto, Formatting = Newtonsoft.Json.Formatting.Indented };
        string jsonString = JsonConvert.SerializeObject(dataCollection, settings);
        PlayerPrefs.SetString("oskardatatester", jsonString);

    }

    private void Load()
    {
        if (PlayerPrefs.HasKey("oskardatatester"))
        {
            string json = PlayerPrefs.GetString("oskardatatester");
            JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto, Formatting = Newtonsoft.Json.Formatting.Indented };
            dataCollection = (JsonConvert.DeserializeObject<OSKARObject_SetupdataCollection>(json, settings));
        }
    }

    private void ClearData()
    {
        dataCollection = new OSKARObject_SetupdataCollection();
        Save();
    }
}
