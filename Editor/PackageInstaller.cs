using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEditor;
using UnityEditor.PackageManager;
using System.IO;

public class PackageInstaller : AssetPostprocessor
{
    private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        CheckFolder();
    }

    [InitializeOnLoadMethod]
    private static void InitializeOnLoad()
    {
        //Gets call("ed when unity loads
        CheckFolder();
    }

    private static void CheckFolder()
    {
        if(!Directory.Exists(Path.Combine(Application.dataPath, "OSKARContent"))){
            Directory.CreateDirectory(Path.Combine(Application.dataPath, "OSKARContent"));
            //Create Build Settings
        }

    }
}