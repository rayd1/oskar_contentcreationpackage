This plugin allows you to build graphics and content for Red Bull OSKAR AR Software. You need to export the objects you created and upload them to the OSKAR CMS in order to use them in your Projects. 

REQUIREMENTS

Open a new Unity Project in the Unity Version currently used by the main OSKAR App, and choose URP for your rendering path. If you create your objects in the standard or HDRP Pipeline, 
you will not be able to use them in OSKAR.

This Package installs all the required other packages needed as dependencies.

To distibute the ojects you created, you need access to the OSKAR CMS.

HOW TO USE

If you want to create objects for OSKAR, there are some things to keep in mind:

- Every Object you want to show needs to be a single Prefab
- You can use whatever Scripts/Shaders/Materials/Animations - everything you can do in Unity is possible. Just be sure to include all the used Assets in the "OSKARContent" Folder
- Do not use ressources or streaming assets Folder
- Everything you build needs to be in URP Pipeline
- Everything you build needs to run on an IPad, so be sure not to use Code that won't work on IOS (reflections...), and optimize the objects polycount and shader fidelity to be able to run on mobile.

BASIC CONCEPTS

Every object you want to have as a single object in OSKAR needs to be a prefab and it needs a Monobehaviour on it which is a child class of "OSKARObjectControllerBase". There is a more high-level Component 
"OSKARObjectController", which inherits from the base class, and you can (and should) just create your own script and inherit from "OSKARObjectController". This is the main connection to the OSKAR App for 
all the controlling and displaying purposes.

Look at the simplem Samples to understand the use of "OSKARObjectController".

There are also a lot of Components already in the package to control the objects you want to build, see the documentation for more information. 

	BASE Component	
	- Every Object needs to have an OSKARObjectController script attached to it. (or a Script inheriting from this class). The OSKAR App searches for this component and sends all the control inputs via this script. Placing, rotating and anchoring is done one level higher and not in the control of the OSKAR Assets.
	
	In-Out
	- The OSKARObject_InOutController is an easy way to handle every object which should react to the "show" property in the control. This script handles setting the object to the correct layer to be visible in preview/live, and sends an Event to all child components implementing the IObjectInOutReceiver interface. 
	- If you want to trigger a simple animation for In/Out, add the OSKARObject_InOutController and the OSKARObject_AnimationInOutController to the object. 
	- If you want to do your custom In-Out logic, you can add your own script inheriting from IObjectInOutReceiver
	- There is also a simple script for fading out the Alpha Value of a shader on In/Out in the package (OSKARObject_ShaderInOutController)
	
	Utility
	There are some scripts useful for most AR Objects in the package:
	- A Billboard controller which lets a gameobject always look towards the camera
	- A Stick to Ground Controller which places a gameobject always on the ground, no matter how high the user places the object (useful for shadows)
	- A rotation controller, which gives a simple rotate around y and swing around y option

	Generic Components for graphics
	For some UI Graphics there are some boilerplate Scripts which can be overridden or copied for your own Elements. See the "UI Samples" Sample in the package for how to use them.
	- Scripts for UIList Elements and UILowerThird Elements
	- Generic Video Plane script
	
	Static Methods
	For some interaction with the Main App there are some statics:
	- GetFlag(string nationCode) => returns a texture of the nation flag
	
EXPORT

When you have created your objects, you need to export them in order to upload them to the OSKAR CMS. Therefore open the Editor Window "OSKAR Content Exporter" (found in the toolbar under "OSKAR").
You need to have some requirements before you can export:

- All your assets, scripts, shaders and also third-party packages/assets need to be in the "OSKARContent" folder, as only this folder will be exported. 
- You have to have a "OSKAR_Build Settings" Object (scriptable object, created via the context menu in the content browser") in the root of this "OSKARContent" folder. This asset defines some basics of the 
  project you want to export
- You need to have an "OSKAR_Object Data" scriptable object for each object you want to export, which defines the externals of the Object and acts as a interface for the controller software.

If you have everything set up, just hit the "export" button and upload the package you exported to the OSKAR CMS. 

